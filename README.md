# xivo-collectd-monitoring

Installs and configures collectd agent.
It also contains scripts for the Exec plugin, but does not install them.

## Prerequisites

Remote Graphite database must be installed.  
`xivo-monitoring.conf` or `xivocc-monitoring.conf` file must exist in the working directory.

## Install script for the Exec plugin

If you want to install a script, edit the `.conf` file before running `install-collectd.sh`.
Add line `LoadPlugin exec` and plugin configuration section:

```
<Plugin exec>
        Exec asterisk "/etc/collectd/exec/asterisk.sh"
</Plugin>
```

Copy the script manually.

## Install on XiVO server

```
./install-collectd.sh xivo-monitoring.conf
```

## Install on XiVO CC server

```
./install-collectd.sh xivocc-monitoring.conf
```

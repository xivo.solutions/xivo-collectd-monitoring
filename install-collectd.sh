#!/usr/bin/env bash
# Copyright (C) 2017 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

check_args() {
    if [ -z $1 ]; then
        cat <<-EOF
		Usage:
		    install-collectd [conf file]
		EOF
        exit 0
    else
        CONFIG_FILE=$1
        if [ ! -f "$CONFIG_FILE" ]; then
            echo "Configuration file '$CONFIG_FILE' not found."
            exit 1
        fi
    fi
}

check_sudo() {
    if [ "$(whoami)" != "root" ]; then
        echo "This script needs to be executed with sudo."
        exit 2
    fi
}

set_repository() {
    echo "deb http://pkg.ci.collectd.org/deb trusty collectd-5.6" > /etc/apt/sources.list.d/pkg.ci.collectd.org.list
    apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 3994D24FB8543576
    apt-get update
}

install_collectd() {
    if [ $(apt-cache policy collectd | grep Candidate | grep -oE ' 5\.6\.[0-9]+') ]; then
        apt-get -y install collectd
    else
        echo "Installation failed. Candidate version of Collectd does not match."
        exit 3
    fi
}

get_hostname() {
    CUSTOM_HOSTNAME=$(whiptail --inputbox "Enter this machine's unique name used for Graphite and Grafana:\nFQDN is $(hostname --fqdn)" 10 50 3>&1 1>&2 2>&3)
    if [ -z $CUSTOM_HOSTNAME ]; then
        echo "Name not set. Aborting the installation."
        exit 4
    fi
    echo "This machine will be referenced as '$CUSTOM_HOSTNAME'."
}

get_db_host() {
    DB_HOST=$(whiptail --inputbox "Enter the Graphite database FQDN or IP (without port number):" 8 50 3>&1 1>&2 2>&3)
    if [ -z $DB_HOST ]; then
        echo "IP not set. Aborting the installation."
        exit 5
    fi
    echo "Using remote database at address '$DB_HOST'."
}

backup_configuration() {
    if [ ! -f "/etc/collectd/collectd.conf.sample" ]; then
        echo "Renaming original configuration file to /etc/collectd/collectd.conf.sample"
        mv -f /etc/collectd/collectd.conf /etc/collectd/collectd.conf.sample
    fi
}

copy_configuration_files() {
    echo "Copying $CONFIG_FILE to /etc/collectd/collectd.conf.d/"
    cp -f $CONFIG_FILE /etc/collectd/collectd.conf.d/
}

rewrite_configuration() {
	echo "Generating configuration file /etc/collectd/collectd.conf"
    echo "Hostname \"$CUSTOM_HOSTNAME\"" > /etc/collectd/collectd.conf
    cat >> /etc/collectd/collectd.conf <<-EOF

	<Include "/etc/collectd/collectd.conf.d">
	    Filter "*.conf"
	</Include>

	<Plugin write_graphite>
	        <Node "monitoring">
	        Host "${DB_HOST}"
	        Port "2003"
	        LogSendErrors true
	    </Node>
	</Plugin>
	EOF
}

restart_collectd() {
    service collectd restart
    service collectd status
}

check_args $@
check_sudo
set_repository
install_collectd
get_hostname
get_db_host
backup_configuration
copy_configuration_files
rewrite_configuration
restart_collectd

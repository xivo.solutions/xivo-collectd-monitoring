HOSTNAME="${COLLECTD_HOSTNAME:-localhost}"
INTERVAL="${COLLECTD_INTERVAL:-10}"

while sleep "$INTERVAL"; do
  xuc_connections=$(asterisk -rx 'manager show connected' | grep xuc | awk '{print $4}')
  asterisk_uptime=$(asterisk -rx 'core show uptime seconds' | grep System | awk '{print $3}')
  active_calls=$(asterisk -rx 'core show calls' | grep 'active calls' | awk '{print $1}')
  processed_calls=$(asterisk -rx 'core show calls' | grep 'calls processed' | awk '{print $1}')

  echo "PUTVAL \"$HOSTNAME/exec-asterisk/gauge-xuc-connections\" interval=$INTERVAL N:$xuc_connections"
  echo "PUTVAL \"$HOSTNAME/exec-asterisk/gauge-asterisk-uptime\" interval=$INTERVAL N:$asterisk_uptime"
  echo "PUTVAL \"$HOSTNAME/exec-asterisk/gauge-active-calls\" interval=$INTERVAL N:$active_calls"
  echo "PUTVAL \"$HOSTNAME/exec-asterisk/gauge-processed-calls\" interval=$INTERVAL N:$processed_calls"
done
